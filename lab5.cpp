/*******************
  Gary Allen
  grallen
  Lab #5
  Lab Section: CPSC 1021-003
  TA: Nushrat Humaira
*******************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));
  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   //these two loops will initialize the deck of cards to be as a deck of Cards
   //should be. One card of each type of each suit.
   //14 for ace, 13 for king, 12 for queen, 11 for jack
   Card deck[52];
   int counter= 0;
   for (int i=0; i < 4; i++) {
     for (int j=2; j<=14; j++) {
       deck[counter].value= j;
       deck[counter].suit =static_cast<Suit>(i);
       counter += 1;
     }
   }
   Card hand[5];
  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   //random_shuffle shuffles the deck of the cards created earlier
   random_shuffle(&deck[0], &deck[52], myrandom);
   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    //will create a random hand based on the randomly shuffled deck from above
    for (int i = 0; i < 5; i++) {
      hand[i]=deck[i];
    }
    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
     //Cards sorted via standard sort function, which uses boolean variables
     //to determine what values are greater than or less than the current value
     std::sort(&hand[0], &hand[5], suit_order);
    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
     //Printing the cards using the get_card_name and get_suit_code functions
     //in order to make sure the proper symbol and name is printed for each
     //card
     for (int i = 0; i < 5; i++) {
       std::string name;
       std::string suit;
       name = get_card_name(hand[i]);
       suit = get_suit_code(hand[i]);
       cout << setw(10) << name << suit << endl;
     }
  return 0;
}
/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
//This function is done to sort the suits and cards
//will return true if the left hand card is less than the right hand card
//otherwise, it will return false
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit) {
    return true;
  }
  else if (lhs.suit > rhs.suit) {
    return false;
  }
  else if (lhs.suit == rhs.suit) {
    if (lhs.value < rhs.value) {
        return true;
    }
    else if (lhs.value > rhs.value) {
        return false;
    }

  }
  else {
    return false;
  }


}
//Returning the string value for the suit of the card provided in parameter
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}
//Returning the string value for the card number provided
string get_card_name(Card& c) {
  std::string name;
  if (c.value == 2) {
    name = "2 of ";
  }
  else if (c.value == 3){
    name = "3 of ";
  }
  else if (c.value == 4){
    name = "4 of ";
  }
  else if (c.value == 5){
    name = "5 of ";
  }
  else if (c.value == 6){
    name = "6 of ";
  }
  else if (c.value == 7){
    name = "7 of ";
  }
  else if (c.value == 8){
    name = "8 of ";
  }
  else if (c.value == 9){
    name = "9 of ";
  }
  else if (c.value == 10){
    name = "10 of ";
  }
  else if (c.value == 11){
    name = "Jack of ";
  }
  else if (c.value == 12){
    name = "Queen of ";
  }
  else if (c.value == 13){
    name = "King of ";
  }
  else if (c.value == 14){
    name = "Ace of ";
  }
  return name;
  }
